
# Incluir configuração
source config.sh

# Iniciar servidor SQLite
sqlite3 "$DATABASE_FILE" &

# Aguardar inicialização do banco de dados
sleep 1

# Criar as tabelas (se necessário)
db_models.sh criar_tabelas clientes "${cliente_table[@]}"
db_models.sh criar_tabelas transacoes "${transacao_table[@]}"
db_models.sh criar_tabelas saldos "${saldo_table[@]}"

# Iniciar loop de processamento de comandos
while true; do
  # Ler comando do SQLite
  comando=$(echo -n "$1" | base64 -d)

  # Executar comando
  sqlite3 "$DATABASE_FILE" "$comando"

done
