   /home/sobrinhosj/backend-rinha/
    ├── app
    │   └── api.sh
    ├── arquitetura.md
    ├── database
    │   ├── config.sh
    │   ├── db_models.sh
    │   ├── init.sql
    │   └── migrations
    │       ├── 0001_initial_schema.sql
    │       └── 0002_add_saldo_table.sql
    ├── docs
    │   └── README.md
    ├── domain
    │   ├── init.sh
    │   ├── models.sh
    │   └── services.sh
    ├── main.sh
    ├── nginx.conf
    ├── README.md
    ├── start-api.sh
    ├── start-database.sh
    └──  tests
        └── unit
            ├── extrato_test.sh
            └── transacoes_test.sh

#------------------------------------------------#

#app/api.sh: Contém a API principal da aplicação, incluindo as rotas e a lógica de negócio.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.2
#!/bin/bash

# Carregar funções auxiliares
source config.sh

# Definir rotas da API
function api_transacoes() {
  case "$1" in
    "POST")
      api_transacoes_post "$2"
      ;;
    *)
      echo "Método HTTP inválido: $1"
      exit 1
      ;;
  esac
}

function api_transacoes_post() {
  cliente_id="$1"
  valor="$2"
  tipo="$3"
  descricao="$4"

  # Validar entrada
  if ! [[ "$cliente_id" =~ ^[0-9]+$ ]]; then
    echo "Cliente inválido: $cliente_id"
    exit 1
  fi
  if ! [[ "$valor" =~ ^[0-9]+$ ]]; then
    echo "Valor inválido: $valor"
    exit 1
  fi
  if ! [[ "$tipo" =~ ^[cd]$ ]]; then
    echo "Tipo inválido: $tipo"
    exit 1
  fi
  if ! [[ "$descricao" =~ ^.{1,10}$ ]]; then
    echo "Descrição inválida: $descricao"
    exit 1
  fi

  # Realizar transação
  transacao_id=$(gerar_id)
  realizada_em=$(date +'%Y-%m-%d %H:%M:%S')
  if realizar_transacao "$cliente_id" "$valor" "$tipo" "$descricao" "$transacao_id" "$realizada_em"; then
    # Retornar sucesso
    saldo=$(obter_saldo "$cliente_id")
    limite=$(obter_limite "$cliente_id")
    echo "{ \"limite\": $limite, \"saldo\": $saldo }"
    exit 0
  else
    # Retornar erro
    echo "Falha ao realizar transação"
    exit 1
  fi
}

function api_extrato() {
  cliente_id="$1"

  # Validar entrada
  if ! [[ "$cliente_id" =~ ^[0-9]+$ ]]; then
    echo "Cliente inválido: $cliente_id"
    exit 1
  fi

  # Obter dados do cliente
  saldo=$(obter_saldo "$cliente_id")
  limite=$(obter_limite "$cliente_id")
  data_extrato=$(date +'%Y-%m-%d %H:%M:%S')

  # Obter últimas transações
  ultimas_transacoes=$(obter_ultimas_transacoes "$cliente_id")

  # Retornar sucesso
  echo "{ \"saldo\": $saldo, \"total\": $saldo, \"data_extrato\": \"$data_extrato\", \"limite\": $limite, \"ultimas_transacoes\": $ultimas_transacoes }"
  exit 0
}

# Iniciar API
while true; do
  # Ler requisição
  requisicao=$(echo -n "$1" | base64 -d)
  metodo=$(echo "$requisicao" | cut -d' ' -f1)
  uri=$(echo "$requisicao" | cut -d' ' -f2)
  parametros=$(echo "$requisicao" | cut -d' ' -f3-)

  # Rotear requisição
  case "$uri" in
    "/clientes/[0-9]+/transacoes")
      api_transacoes "$metodo" "$parametros"
      ;;
    "/clientes/[0-9]+/extrato")
      api_extrato "$parametros"
      ;;
    *)
      echo "URI inválida: $uri"
      exit 1
      ;;
  esac
done


# Funções para acesso ao banco de dados (substituir por implementação real)

function obter_saldo {
  echo "1000" # Implementar consulta ao banco de dados
}

function obter_limite {
  echo "2000" # Implementar consulta ao banco de dados
}

function atualizar_saldo {
  echo "Saldo atualizado com sucesso!" # Implementar atualização no banco de dados
}

function registrar_transacao {
  echo "Transação registrada com sucesso!" # Implementar registro no banco de dados
}

function obter_ultimas_transacoes {
  echo "[]" # Implementar consulta das últimas transacoes no banco de dados
}

# Iniciar servidor Nginx
nginx -c nginx.conf

# Iniciar loop de processamento de requisições
while true; do
  # Ler requisição do Nginx

#------------------------------------------------#

#database/config.sh: Armazena as configurações de acesso ao banco de dados.
# config.sh
# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.2

# Escola c & sqlite SQLite Master
PRAGMA listen_addresses = '*'
PRAGMA effective_cache_size = 300MB
PRAGMA maintenance_work_mem = 25MB
PRAGMA random_page_cost = 1.1
PRAGMA effective_io_concurrency = 30
PRAGMA work_mem = 4MB
PRAGMA huge_pages = off
PRAGMA fsync = off
PRAGMA synchronous = NORMAL;
PRAGMA journal_mode = WAL;
PRAGMA threads = 32;
PRAGMA temp_store = MEMORY;
PRAGMA page_size = 32768;
PRAGMA locking_mode=EXCLUSIVE;
PRAGMA mmap_size = 30000000000;
PRAGMA cache_size=1000000;








#------------------------------------------------#

#database/db_models.sh: Define os modelos de dados utilizados no sistema.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.2

#!/bin/bash
# Define table structure

cliente_table=(
  id:integer primary key
  nome:string:50 not null
  limite:integer
  saldo:integer default 0
)

transacao_table=(
  id:integer primary key
  cliente_id:integer foreign key references clientes(id)
  valor:integer
  tipo:string:1
  descricao:string:10
  realizada_em:datetime default "$(date +'%Y-%m-%d %H:%M:%S')"
)

saldo_table=(
  id:integer primary key
  cliente_id:integer foreign key references clientes(id)
  valor:integer
)

# Define helper functions for interacting with tables

function create_table {
  local table_name="$1"
  local columns="$2"
  sqlite3 "$DATABASE_FILE" "CREATE TABLE $table_name ($columns);"
}

function insert_data {
  local table_name="$1"
  local values="$2"
  sqlite3 "$DATABASE_FILE" "INSERT INTO $table_name VALUES ($values);"
}

function get_data {
  local table_name="$1"
  local condition="$2"
  sqlite3 "$DATABASE_FILE" "SELECT * FROM $table_name WHERE $condition;"
}

function update_data {
  local table_name="$1"
  local values="$2"
  local condition="$3"
  sqlite3 "$DATABASE_FILE" "UPDATE $table_name SET $values WHERE $condition;"
}

# Create tables

create_table clientes "${cliente_table[@]}"
create_table transacoes "${transacao_table[@]}"
create_table saldos "${saldo_table[@]}"

# Insert initial data (replace with your actual data)

insert_data clientes "1, 'Cliente 1', 1000, 0"
insert_data clientes "2, 'Cliente 2', 500, 0"
insert_data clientes "3, 'Cliente 3', 2000, 0"
insert_data clientes "4, 'Cliente 4', 1500, 0"
insert_data clientes "5, 'Cliente 5', 800, 0"

# Load your specific application logic using these helper functions


#------------------------------------------------#

#database/init.sql: Script SQL para inicializar o banco de dados.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.3

# Configuração de MVCC e outros recursos
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
PRAGMA temp_store = MEMORY;
PRAGMA cache_size = 10000;
PRAGMA pool_size = 10;
PRAGMA encoding = "UTF-8";
PRAGMA page_size = 4096;
PRAGMA vacuum_mode = incremental;

# Multi-tenancy
CREATE TABLE IF NOT EXISTS tenants (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  tenant_id INTEGER NOT NULL,
  username TEXT NOT NULL,
  password TEXT NOT NULL,
  FOREIGN KEY (tenant_id) REFERENCES tenants(id)
);

# Tabela de clientes
CREATE TABLE IF NOT EXISTS clientes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nome TEXT NOT NULL,
  cpf TEXT NOT NULL,
  saldo INTEGER NOT NULL DEFAULT 0,
  limite INTEGER NOT NULL,
  CONSTRAINT saldo_maior_que_o_limite CHECK (saldo >= (limite * -1))
);

# Tabela de transações
CREATE TABLE IF NOT EXISTS transacoes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  cliente_id INTEGER NOT NULL,
  valor REAL NOT NULL,
  tipo TEXT NOT NULL CHECK (tipo IN ('c', 'd')),
  descricao TEXT NOT NULL,
  data DATETIME NOT NULL DEFAULT (datetime('now')),
  FOREIGN KEY (cliente_id) REFERENCES clientes(id)
);

# Índices
CREATE INDEX idx_clientes_cpf ON clientes (cpf);
CREATE INDEX idx_transacoes_cliente_id ON transacoes (cliente_id);
CREATE INDEX idx_transacoes_data ON transacoes (data);

# Gatilho para atualização de saldo
CREATE TRIGGER atualiza_saldo AFTER INSERT ON transacoes
BEGIN
  UPDATE clientes
  SET saldo = saldo + CASE tipo
    WHEN 'c' THEN valor
    WHEN 'd' THEN -valor
  END
  WHERE id = cliente_id;
END;

# Inserções de exemplo
INSERT INTO tenants (name) VALUES ('Minha Empresa');

INSERT INTO users (tenant_id, username, password) VALUES (1, 'admin', 'senha123');

INSERT INTO clientes
VALUES
  (1, 'Fulano', '12345678900', 0, 1000),
  (2, 'Ciclano', '98765432100', 0, 800),
  (3, 'Beltrano', '00000000000', 0, 10000),
  (4, 'Cicrano', '11111111111', 0, 100000),
  (5, 'Belano', '22222222222', 0, 5000);

INSERT INTO transacoes
VALUES
  (1, 1, 10, 'c', 'Compra'),
  (2, 2, 5, 'd', 'Saque');

# Tabela para gerar IDs sequenciais
CREATE TABLE IF NOT EXISTS ids (
  id INTEGER PRIMARY KEY AUTOINCREMENT


#------------------------------------------------#

#domain/init.sh: Inicializa o módulo de domínio da aplicação.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.2

# Configuração de MVCC e outros recursos
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
PRAGMA temp_store = MEMORY;
PRAGMA cache_size = 10000;
PRAGMA pool_size = 10;
PRAGMA encoding = "UTF-8";
PRAGMA page_size = 4096;
PRAGMA vacuum_mode = incremental;

# Multi-tenancy
CREATE TABLE IF NOT EXISTS tenants (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  tenant_id INTEGER NOT NULL,
  username TEXT NOT NULL,
  password TEXT NOT NULL,
  FOREIGN KEY (tenant_id) REFERENCES tenants(id)
);

# Tabela de clientes
CREATE TABLE IF NOT EXISTS clientes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nome TEXT NOT NULL,
  cpf TEXT NOT NULL,
  saldo INTEGER NOT NULL DEFAULT 0,
  limite INTEGER NOT NULL,
  CONSTRAINT saldo_maior_que_o_limite CHECK (saldo >= (limite * -1))
);

# Tabela de transações
CREATE TABLE IF NOT EXISTS transacoes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  cliente_id INTEGER NOT NULL,
  valor REAL NOT NULL,
  tipo TEXT NOT NULL CHECK (tipo IN ('c', 'd')),
  descricao TEXT NOT NULL,
  data DATETIME NOT NULL DEFAULT (datetime('now')),
  FOREIGN KEY (cliente_id) REFERENCES clientes(id)
);

# Índices
CREATE INDEX idx_clientes_cpf ON clientes (cpf);
CREATE INDEX idx_transacoes_cliente_id ON transacoes (cliente_id);
CREATE INDEX idx_transacoes_data ON transacoes (data);

# Gatilho para atualização de saldo
CREATE TRIGGER atualiza_saldo AFTER INSERT ON transacoes
BEGIN
  UPDATE clientes
  SET saldo = saldo + CASE tipo
    WHEN 'c' THEN valor
    WHEN 'd' THEN -valor
  END
  WHERE id = cliente_id;
END;

# Inserções de exemplo
INSERT INTO tenants (name) VALUES ('Minha Empresa');

INSERT INTO users (tenant_id, username, password) VALUES (1, 'admin', 'senha123');

INSERT INTO clientes
VALUES
  (1, 'Fulano', '12345678900', 0, 1000),
  (2, 'Ciclano', '98765432100', 0, 800),
  (3, 'Beltrano', '00000000000', 0, 10000),
  (4, 'Cicrano', '11111111111', 0, 100000),
  (5, 'Belano', '22222222222', 0, 5000);

INSERT INTO transacoes
VALUES
  (1, 1, 10, 'c', 'Compra'),
  (2, 2, 5, 'd', 'Saque');

# Tabela para gerar IDs sequenciais
CREATE TABLE IF NOT EXISTS ids (
  id INTEGER PRIMARY KEY AUTOINCREMENT
);

INSERT INTO ids DEFAULT VALUES;
SELECT last_insert_rowid() AS novo_id;


#------------------------------------------------#

#domain/models.sh: Define os modelos de domínio da aplicação.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.2

#!/bin/bash

# Incluir configuração
source config.sh

# Definir funções auxiliares

function criar_tabelas {
  tabela="$1"
  colunas="$2"
  sqlite3 "$DATABASE_FILE" "CREATE TABLE IF NOT EXISTS $tabela ($colunas);"
}

function inserir_dados {
  tabela="$1"
  valores="$2"
  sqlite3 "$DATABASE_FILE" "INSERT INTO $tabela VALUES ($valores);"
}

function obter_dados {
  tabela="$1"
  condicao="$2"
  sqlite3 "$DATABASE_FILE" "SELECT * FROM $tabela WHERE $condicao;"
}

function atualizar_dados {
  tabela="$1"
  valores="$2"
  condicao="$3"
  sqlite3 "$DATABASE_FILE" "UPDATE $tabela SET $valores WHERE $condicao;"
}

# Definir estruturas de tabelas (substituir pelas definições reais do modelo, se necessário)

function get_estrutura_tabela_cliente {
  echo "id:inteiro primary key, nome:texto:50 not null, limite:inteiro, saldo:inteiro default 0"
}

function get_estrutura_tabela_transacao {
  echo "id:inteiro primary key, cliente_id:inteiro foreign key references clientes(id), valor:real, tipo:texto:1, descricao:texto:10, realizada_em:datahora default \"$(date +'%Y-%m-%d %H:%M:%S')\""
}

function get_estrutura_tabela_saldo {
  echo "id:inteiro primary key, cliente_id:inteiro foreign key references clientes(id), valor:inteiro"
}

# Exportar funções para uso em outros scripts

export -f criar_tabelas inserir_dados obter_dados atualizar_dados get_estrutura_tabela_cliente get_estrutura_tabela_transacao get_estrutura_tabela_saldo


#------------------------------------------------#

#main.sh: Script principal que inicia a aplicação.

#!/bin/bash

# Incluir configuração
source config.sh

# Iniciar serviços
./start-api.sh
./start-database.sh

# Aguardar término dos serviços
wait

# Exibir mensagem de finalização
echo "API finalizada!"

#------------------------------------------------#

transacoes_test.sh

#------------------------------------------------#

extrato_test.sh

#------------------------------------------------#

services.sh: Contém os serviços utilizados pelo módulo de domínio.

#------------------------------------------------#

