
# Incluir configuração
source config.sh

# Iniciar servidor Nginx
nginx -c nginx.conf

# Iniciar loop de processamento de requisições
while true; do
  # Ler requisição do Nginx
  requisicao=$(echo -n "$1" | base64 -d)
  metodo=$(echo "$requisicao" | cut -d' ' -f1)
  uri=$(echo "$requisicao" | cut -d' ' -f2)
  parametros=$(echo "$requisicao" | cut -d' ' -f3-)

  # Rotear requisição
  case "$uri" in
    "/clientes/[0-9]+/transacoes")
      api_transacoes "$metodo" "$parametros"
      ;;
    "/clientes/[0-9]+/extrato")
      api_extrato "$parametros"
      ;;
    *)
      echo "URI inválida: $uri"
      exit 1
      ;;
  esac
done
