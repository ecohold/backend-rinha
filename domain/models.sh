#domain/models.sh: Define os modelos de domínio da aplicação.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.2

#!/bin/bash

# Incluir configuração
source config.sh

# Definir funções auxiliares

function criar_tabelas {
  tabela="$1"
  colunas="$2"
  sqlite3 "$DATABASE_FILE" "CREATE TABLE IF NOT EXISTS $tabela ($colunas);"
}

function inserir_dados {
  tabela="$1"
  valores="$2"
  sqlite3 "$DATABASE_FILE" "INSERT INTO $tabela VALUES ($valores);"
}

function obter_dados {
  tabela="$1"
  condicao="$2"
  sqlite3 "$DATABASE_FILE" "SELECT * FROM $tabela WHERE $condicao;"
}

function atualizar_dados {
  tabela="$1"
  valores="$2"
  condicao="$3"
  sqlite3 "$DATABASE_FILE" "UPDATE $tabela SET $valores WHERE $condicao;"
}

# Definir estruturas de tabelas (substituir pelas definições reais do modelo, se necessário)

function get_estrutura_tabela_cliente {
  echo "id:inteiro primary key, nome:texto:50 not null, limite:inteiro, saldo:inteiro default 0"
}

function get_estrutura_tabela_transacao {
  echo "id:inteiro primary key, cliente_id:inteiro foreign key references clientes(id), valor:real, tipo:texto:1, descricao:texto:10, realizada_em:datahora default \"$(date +'%Y-%m-%d %H:%M:%S')\""
}

function get_estrutura_tabela_saldo {
  echo "id:inteiro primary key, cliente_id:inteiro foreign key references clientes(id), valor:inteiro"
}

# Exportar funções para uso em outros scripts

export -f criar_tabelas inserir_dados obter_dados atualizar_dados get_estrutura_tabela_cliente get_estrutura_tabela_transacao get_estrutura_tabela_saldo
