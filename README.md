#README.md: README geral da aplicação.

## Backend Rinha - Desafie Seu Domínio (2024/Q1)

Seja bem-vindo(a) ao Backend Rinha, um desafio de programação projetado para impulsionar seu conhecimento e habilidades em controle de concorcorrência! Esta segunda edição lança o desafio, convidando você a desenvolver uma API HTTP robusta que atenda aos seguintes requisitos:

**Funcionalidade Principal:**

- **Transações:**
  - Endereço: `POST /clientes/[id]/transacoes`
  - Corpo da Requisição:
    ```json
    {
      "valor": 1000,  // Inteiro positivo representando centavos (sem decimais)
      "tipo": "c" (crédito) ou "d" (débito),
      "descricao": String (1-10 caracteres)
    }
    ```
  - Resposta (`HTTP 200 OK`):
    ```json
    {
      "limite": 100000,  // Limite registrado do cliente
      "saldo": -9098      // Novo saldo após a transação
    }
    ```
  - **Regras:**
    - Débitos não podem reduzir o saldo do cliente abaixo do limite.
    - A falha em cumprir esta regra resulta em `HTTP 422 Unprocessable Entity` sem alterar o saldo.
    - IDs de clientes inexistentes acionam `HTTP 404 Not Found` (sem corpo necessário).
    - Evite respostas na faixa de 2XX indicando clientes ausentes.
- **Extrato:**
  - Endereço: `GET /clientes/[id]/extrato`
  - Resposta (`HTTP 200 OK`):
    ```json
    {
      "saldo": {
        "total": -9098,       // Saldo total atual
        "data_extrato": "...", // Data/hora da solicitação do extrato (UTC)
        "limite": 100000      // Limite registrado do cliente
      },
      "ultimas_transacoes": [
        { ... detalhes da transação ... }, // Até 10 últimas transações em ordem decrescente
        ...
      ]
    }
    ```
  - **Regras:**
    - IDs de clientes inexistentes acionam `HTTP 404 Not Found` (sem corpo necessário).

**Considerações Cruciais:**

- **Concorrência:** O teste se concentra fortemente em operações simultâneas, enfatizando a importância de um tratamento robusto. Projete sua API para suportar solicitações simultâneas de alto volume, mantendo a integridade dos dados.
- **Corpos de Resposta Concisos:** Os corpos de resposta para solicitações bem-sucedidas devem incluir apenas os dados necessários, seguindo as melhores práticas para eficiência.

**Dados Iniciais do Cliente:**

Para fins de teste, inclua os seguintes clientes pré-registrados (IDs, limites e saldos iniciais):

| ID | Limite         | Saldo Inicial |
|----|---------------|-----------------|
| 1  | 100.000        | R$ 0,00         |
| 2  |  80.000        | R$ 0,00         |
| 3  | 1.000.000      | R$ 0,00         |
| 4  | 10.000.000     | R$ 0,00         |
| 5  |  500.000        | R$ 0,00         |

**Observação Importante:** Não crie um cliente com ID 6. O teste pretende verificar o tratamento bem-sucedido de IDs de clientes inexistentes por meio de `HTTP 404 Not Found`.

**Diretrizes de Envio:**

- **Dockerize:** Empacote sua API e componentes necessários usando Docker Compose, respeitando as restrições de CPU e memória (veja "Restrições de Recursos" abaixo).
- **Requisitos Mínimos de Arquitetura:**
  - Balanceador de carga usando distribuição round-robin (não limitado ao Nginx; considere HAProxy ou outros).
  - Duas instâncias do servidor HTTP atrás do balanceador de carga.
  - Banco de dados relacional ou não relacional (excluindo opções baseadas em memória como Redis).
  - Sinta-se à vontade para incluir componentes adicionais, mas certifique-se de que as restrições de recursos sejam atendidas (evite combinações desnecessárias como um banco de dados relacional e