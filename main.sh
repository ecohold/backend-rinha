#main.sh: Script principal que inicia a aplicação.


#!/bin/bash

# Inicializar banco de dados
sqlite3 database.db < database/init.sql

# Iniciar serviços
source start-api.sh
source start-database.sh

# Aguardar término dos serviços
wait

# Exibir mensagem de finalização
echo "API finalizada!"