#database/db_models.sh: Define os modelos de dados utilizados no sistema.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.2

#!/bin/bash
# Define table structure

cliente_table=(
  id:integer primary key
  nome:string:50 not null
  limite:integer
  saldo:integer default 0
)

transacao_table=(
  id:integer primary key
  cliente_id:integer foreign key references clientes(id)
  valor:integer
  tipo:string:1
  descricao:string:10
  realizada_em:datetime default "$(date +'%Y-%m-%d %H:%M:%S')"
)

saldo_table=(
  id:integer primary key
  cliente_id:integer foreign key references clientes(id)
  valor:integer
)

# Define helper functions for interacting with tables

function create_table {
  local table_name="$1"
  local columns="$2"
  sqlite3 "$DATABASE_FILE" "CREATE TABLE $table_name ($columns);"
}

function insert_data {
  local table_name="$1"
  local values="$2"
  sqlite3 "$DATABASE_FILE" "INSERT INTO $table_name VALUES ($values);"
}

function get_data {
  local table_name="$1"
  local condition="$2"
  sqlite3 "$DATABASE_FILE" "SELECT * FROM $table_name WHERE $condition;"
}

function update_data {
  local table_name="$1"
  local values="$2"
  local condition="$3"
  sqlite3 "$DATABASE_FILE" "UPDATE $table_name SET $values WHERE $condition;"
}

# Create tables

create_table clientes "${cliente_table[@]}"
create_table transacoes "${transacao_table[@]}"
create_table saldos "${saldo_table[@]}"

# Insert initial data (replace with your actual data)

insert_data clientes "1, 'Cliente 1', 1000, 0"
insert_data clientes "2, 'Cliente 2', 500, 0"
insert_data clientes "3, 'Cliente 3', 2000, 0"
insert_data clientes "4, 'Cliente 4', 1500, 0"
insert_data clientes "5, 'Cliente 5', 800, 0"

# Load your specific application logic using these helper functions
