#database/config.sh: Armazena as configurações de acesso ao banco de dados.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.3

# Configurações de conexão (opcional)
# Se estas configurações forem usadas, comente o comando "source config.sh" em main.sh
#database_path="/caminho/para/database.db"
#database_username="usuario"
#database_password="senha"
