#database/init.sql: Script SQL para inicializar o banco de dados.

# author: zeh sobrinho & tutorC & gemini & sapiens chat &
# date: 10-03-24
# versão: 1.0.3

# Configuração de MVCC e outros recursos
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
PRAGMA temp_store = MEMORY;
PRAGMA cache_size = 10000;
PRAGMA pool_size = 10;
PRAGMA encoding = "UTF-8";
PRAGMA page_size = 4096;
PRAGMA vacuum_mode = incremental;

# Multi-tenancy
CREATE TABLE IF NOT EXISTS tenants (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  tenant_id INTEGER NOT NULL,
  username TEXT NOT NULL,
  password TEXT NOT NULL,
  FOREIGN KEY (tenant_id) REFERENCES tenants(id)
);

# Tabela de clientes
CREATE TABLE IF NOT EXISTS clientes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nome TEXT NOT NULL,
  cpf TEXT NOT NULL,
  saldo INTEGER NOT NULL DEFAULT 0,
  limite INTEGER NOT NULL,
  CONSTRAINT saldo_maior_que_o_limite CHECK (saldo >= (limite * -1))
);

# Tabela de transações
CREATE TABLE IF NOT EXISTS transacoes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  cliente_id INTEGER NOT NULL,
  valor REAL NOT NULL,
  tipo TEXT NOT NULL CHECK (tipo IN ('c', 'd')),
  descricao TEXT NOT NULL,
  data DATETIME NOT NULL DEFAULT (datetime('now')),
  FOREIGN KEY (cliente_id) REFERENCES clientes(id)
);

# Índices
CREATE INDEX idx_clientes_cpf ON clientes (cpf);
CREATE INDEX idx_transacoes_cliente_id ON transacoes (cliente_id);
CREATE INDEX idx_transacoes_data ON transacoes (data);

# Gatilho para atualização de saldo
CREATE TRIGGER atualiza_saldo AFTER INSERT ON transacoes
BEGIN
  UPDATE clientes
  SET saldo = saldo + CASE tipo
    WHEN 'c' THEN valor
    WHEN 'd' THEN -valor
  END
  WHERE id = cliente_id;
END;

# Inserções de exemplo
INSERT INTO tenants (name) VALUES ('Minha Empresa');

INSERT INTO users (tenant_id, username, password) VALUES (1, 'admin', 'senha123');

INSERT INTO clientes
VALUES
  (1, 'Fulano', '12345678900', 0, 1000),
  (2, 'Ciclano', '98765432100', 0, 800),
  (3, 'Beltrano', '00000000000', 0, 10000),
  (4, 'Cicrano', '11111111111', 0, 100000),
  (5, 'Belano', '22222222222', 0, 5000);

INSERT INTO transacoes
VALUES
  (1, 1, 10, 'c', 'Compra'),
  (2, 2, 5, 'd', 'Saque');

# Tabela para gerar IDs sequenciais
CREATE TABLE IF NOT EXISTS ids (
  id INTEGER PRIMARY KEY AUTOINCREMENT
