1. Abra o SQLite:

Abra um terminal e digite o seguinte comando para iniciar o interpretador SQLite:

sqlite3


sobrinhosj@cs-610301933215-default:~/backend-rinha$ sqlite3
SQLite version 3.34.1 2021-01-20 14:10:07
Enter ".help" for usage hints.
Connected to a transient in-memory database.
Use ".open FILENAME" to reopen on a persistent database.
sqlite> 
 
A mensagem do SQLite indica que você conectou com sucesso ao banco de dados in-memory:

    "SQLite version 3.34.1 2021-01-20 14:10:07": Informa a versão do SQLite que você está utilizando (3.34.1) e a data de compilação (20 de janeiro de 2021).
    "Enter '.help' for usage hints.": É uma dica informativa sugerindo que você digite ".help" para obter auxílio sobre o uso do interpretador de comandos SQLite.
    "Connected to a transient in-memory database.": Informa que você está conectado a um banco de dados temporário na memória. Isso significa que os dados criados aqui serão perdidos quando você fechar o interpretador SQLite.
    "Use '.open FILENAME' to reopen on a persistent database.": É uma dica informativa explicando que, se desejar usar um banco de dados persistente (que armazene dados mesmo após fechar o SQLite), você pode utilizar o comando ".open FILENAME" seguido do nome do arquivo do banco de dados.
    "sqlite>": É o prompt de comando do interpretador SQLite, indicando que ele está pronto para receber comandos.

Resumindo, a mensagem do SQLite confirma a conexão bem-sucedida ao banco de dados in-memory e fornece dicas sobre como obter ajuda e usar bancos de dados persistentes.

2. Execute cada linha separadamente:

Linha 1:
SQL

PRAGMA journal_mode = WAL;

Use o código com cuidado.

Pressione Enter. Verifique se há alguma mensagem de erro. Se não houver erro, siga para a próxima linha.

Linha 2:
SQL

PRAGMA synchronous = NORMAL;

Use o código com cuidado.

Execute esse comando e verifique se há erros.

Continue repetindo o processo para todas as linhas até o final do arquivo init.sql.